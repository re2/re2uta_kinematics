/*
 * CenterOfMassControllerTest.cpp
 *
 *  Created on: Mar 4, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/CenterOfMassController.hpp>
#include <re2uta/calc_point_mass.hpp>


#include <re2uta/center_of_mass_jacobian.hpp>
#include <re2uta/kinematics.hpp>
#include <re2uta/pinv.hpp>
#include <re2/kdltools/kdl_tools.h>
#include <re2/visutils/VisualDebugPublisher.h>

#include <robot_state_publisher/robot_state_publisher.h>
#include <tf/transform_broadcaster.h>

#include <interactive_markers/interactive_marker_server.h>

#include <sensor_msgs/JointState.h>
#include <visualization_msgs/Marker.h>

#include <ros/ros.h>

#include <eigen_conversions/eigen_kdl.h>

#include <kdl/tree.hpp>
#include <kdl/treefksolverpos_recursive.hpp>
#include <kdl_parser/kdl_parser.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>

#include <iostream>
#include <fstream>



boost::shared_ptr<re2::VisualDebugPublisher> vizDebugPublisher;


class CenterOfMassControllerTest
{
    private:
        typedef std::map<std::string, double> JointPositionMap;

        ros::NodeHandle  m_node;
        KDL::Tree        m_tree;
        KDL::JntArray    m_jointPositions;
        JointPositionMap m_jointPositionMap;

        tf::TransformBroadcaster                     m_broadcaster;
        interactive_markers::InteractiveMarkerServer m_interactiveMarkerServer;



    public:
        CenterOfMassControllerTest()
            : m_interactiveMarkerServer( "simple" )
        {
            using namespace re2::drc::kinematics;

            std::string urdfFileName = "simple_arm.urdf" ;
            m_node.getParam( "urdf_file_name", urdfFileName );

            std::ifstream simpleUrdfFile( urdfFileName.c_str() );
            std::string   simpleUrdfString(  (std::istreambuf_iterator<char>(simpleUrdfFile)),
                                             (std::istreambuf_iterator<char>()              )  );

            m_node.setParam( "/robot_description", simpleUrdfString );

            kdl_parser::treeFromString( simpleUrdfString, m_tree );
            printTree( m_tree.getRootSegment()->second );


            m_jointPositions.resize( m_tree.getNrOfJoints() );
            ROS_INFO_STREAM( "Says we have " << m_tree.getNrOfJoints() << " joints" );

            int index = 0;
            BOOST_FOREACH( KDL::SegmentMap::value_type const & entry, m_tree.getSegments() )
            {
                KDL::Segment const & segment = entry.second.segment;
                m_jointPositionMap[ segment.getJoint().getName() ] = 0.0;
                m_jointPositions( entry.second.q_nr ) = 0.0;

                ++index;
            }

            m_jointPositions( m_tree.getSegment( "l_lleg"  )->second.q_nr ) =  0.4;
            m_jointPositions( m_tree.getSegment( "r_lleg"  )->second.q_nr ) =  0.4;
            m_jointPositions( m_tree.getSegment( "l_talus" )->second.q_nr ) = -0.2;
            m_jointPositions( m_tree.getSegment( "r_talus" )->second.q_nr ) = -0.2;
            m_jointPositions( m_tree.getSegment( "l_uleg"  )->second.q_nr ) = -0.2;
            m_jointPositions( m_tree.getSegment( "r_uleg"  )->second.q_nr ) = -0.2;


            // create an interactive marker for our server
            visualization_msgs::InteractiveMarker int_marker;
            int_marker.header.frame_id = "/l_foot";
            int_marker.name            = "my_marker";
            int_marker.description     = "Simple 1-DOF Control";
            int_marker.scale           = 0.1;

            // create a grey box marker
            visualization_msgs::Marker box_marker;
            box_marker.type = visualization_msgs::Marker::CUBE;
            box_marker.scale.x = 0.05;
            box_marker.scale.y = 0.05;
            box_marker.scale.z = 0.05;
            box_marker.color.r = 0.5;
            box_marker.color.g = 0.5;
            box_marker.color.b = 0.5;
            box_marker.color.a = 1.0;

            // create a non-interactive control which contains the box
            visualization_msgs::InteractiveMarkerControl box_control;
            box_control.always_visible = true;
            box_control.markers.push_back( box_marker );


            int_marker.controls.push_back( box_control );


            visualization_msgs::InteractiveMarkerControl rotate_control;
            rotate_control.name = "rotate_x";
            rotate_control.interaction_mode =
            visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;

            // add the control to the interactive marker
            int_marker.controls.push_back(rotate_control);

            // add the interactive marker to our collection &
            // tell the server to call processFeedback() when feedback arrives for it
            m_interactiveMarkerServer.insert(int_marker);//, &CenterOfMassControllerTest::processFeedback, this );

            // 'commit' changes and send to all clients
            m_interactiveMarkerServer.applyChanges();

        }


        void processFeedback(  const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
        {
            ROS_INFO_STREAM( feedback->marker_name << " is now at " <<
                             feedback->pose.position.x << ", " <<
                             feedback->pose.position.y << ", " <<
                             feedback->pose.position.z );
        }


        void go()
        {
            re2::drc::CenterOfMassController           centerOfMassController( m_tree );
            robot_state_publisher::RobotStatePublisher robotStatePublisher( m_tree );
            vizDebugPublisher.reset( new re2::VisualDebugPublisher( "jacobian_markers", "l_foot" ) );


            const KDL::TreeElement & leftFootElement    = m_tree.getSegment( "l_foot" )->second;
//            Eigen::Vector6d          originalRFootPose  = Eigen::Vector6d::Ones() * std::numeric_limits<double>::quiet_NaN();
            Eigen::Vector6d          originalRFootPose  = centerOfMassController.getPose("l_foot","r_foot", m_jointPositions.data );
            Eigen::Vector6d          originalPelvisPose = centerOfMassController.getPose("l_foot","pelvis", m_jointPositions.data );
            Eigen::Vector3d          originalRFootLoc   = originalRFootPose.head<3>();

            Eigen::Vector4d originalPointMass;
            originalPointMass = calcPointMass( leftFootElement, m_jointPositions.data );

            int counter = 0;
            while( ros::ok() )
            {
                Eigen::Vector4d currentPointMass;
                currentPointMass = calcPointMass( leftFootElement, m_jointPositions.data );

                Eigen::Vector3d currentCom( currentPointMass.head<3>() );


//                Eigen::Affine3d  rFootXform = centerOfMassController.getTransform( "l_foot", "r_foot", m_jointPositions.data );
                Eigen::Vector6d  rFootPose  = centerOfMassController.getPose( "l_foot", "r_foot", m_jointPositions.data );
//                Eigen::Vector3d  rFootLoc   = rFootPose.translation();
                Eigen::Vector3d  rFootLoc   = rFootPose.head<3>();


                double radians = M_PI/1000 * counter;

                Eigen::Vector3d targetPoint;
                targetPoint      = originalPointMass.head<3>();
//                targetPoint.x()  = 0; //std::sin(counter * M_PI/100)*0.05;
                targetPoint.y() += std::cos(radians)*0.1;
//                targetPoint.z() -= 0.1;

                Eigen::Vector3d comDesiredVelocity( 0,0,0 );
                comDesiredVelocity = 0.001 * (targetPoint - currentCom) ;


                Eigen::Vector6d footTarget;
                footTarget            = originalRFootPose;
//                footTarget.head<3>() += Eigen::Vector3d( std::cos(radians)*0.05, std::sin(radians)*0.05, 0 );

                Eigen::Vector6d desiredFootTwist( Eigen::Vector6d::Zero() );
                desiredFootTwist = 0.001 * (footTarget - rFootPose) ;
//                desiredFootTwist.tail<3>() = Eigen::Vector3d(0,0,0) ;

                Eigen::VectorXd jointVelocityVector;
                jointVelocityVector = centerOfMassController.calcVelocitiesForComCommand( m_jointPositions.data, comDesiredVelocity, desiredFootTwist );

                Eigen::Vector3d expectedComVelocity = centerOfMassController.m_lastComJacobianFromLFoot    * jointVelocityVector * 50;
                Eigen::Vector6d expectedRFootTwist  = centerOfMassController.m_lastTipJacobianRFootInLFoot * jointVelocityVector * 50;

                BOOST_FOREACH( KDL::SegmentMap::value_type const & entry, m_tree.getSegments() )
                {
                    const KDL::Segment  & segment      = entry.second.segment;

                    if( segment.getJoint().getType() != KDL::Joint::None )
                    {
                        int jointId = entry.second.q_nr;
                        Eigen::Vector3d rotationAxis;
                        rotationAxis = Eigen::Map<Eigen::Vector3d>( segment.getJoint().JointAxis().data );
                        rotationAxis.normalize();

                        Eigen::Vector3d  scaledRotationAxis = rotationAxis * jointVelocityVector(jointId); // / 10.0;
                        Eigen::Affine3d  jointPose          = centerOfMassController.getTransform( "l_foot", segment.getName(), m_jointPositions.data );
                        vizDebugPublisher->publishVectorFrom( Eigen::Vector3d( jointPose.translation()                   ),
                                                              Eigen::Vector3d( jointPose.rotation() * scaledRotationAxis ),
                                                              Eigen::Vector4d(0,1,0,1),
                                                              7000+jointId, "l_foot" );

                        m_jointPositions(jointId)                          += jointVelocityVector(jointId);
                        m_jointPositionMap[ segment.getJoint().getName() ]  = m_jointPositions(jointId);
                    }

                }

//                vizDebugPublisher->publishSegment(        currentCom,                  targetPoint, Eigen::Vector4d( 1.0, 0.0, 1.0, 1 ), 1, "l_foot" );
//                vizDebugPublisher->publishSegment(  originalRFootLoc,                     rFootLoc, Eigen::Vector4d( 1.0, 0.5, 1.0, 1 ), 2, "l_foot" );
//                vizDebugPublisher->publishSegment(          rFootLoc,         footTarget.head<3>(), Eigen::Vector4d( 0.5, 0.5, 1.0, 1 ), 3, "l_foot" );
                vizDebugPublisher->publishVectorFrom(     currentCom,          expectedComVelocity, Eigen::Vector4d( 0.5, 0.5, 0.5, 1 ), 4, "l_foot" );
                vizDebugPublisher->publishVectorFrom(       rFootLoc, expectedRFootTwist.head<3>(), Eigen::Vector4d( 0.5, 0.5, 0.5, 1 ), 5, "l_foot" );
                vizDebugPublisher->publishPoint3(    currentPointMass.head<3>(), Eigen::Vector4d( 1.0, 0.5, 0.5, 1 ), 6, "l_foot" );
                vizDebugPublisher->publishPoint3(   originalPointMass.head<3>(), Eigen::Vector4d( 0.5, 1.0, 0.5, 1 ), 7, "l_foot" );
                vizDebugPublisher->publishPoint3(       targetPoint, Eigen::Vector4d( 0.5, 0.5, 1.0, 1 ), 7, "l_foot" );


                ros::Duration(0.002).sleep();
                robotStatePublisher.publishFixedTransforms();
                robotStatePublisher.publishTransforms( m_jointPositionMap, ros::Time::now() );

                ++counter;
            }
        }

};


int main( int argc, char ** argv )
{
    ros::init( argc, argv, "re2_drc_kinematic_test" );
    ros::NodeHandle node;

    CenterOfMassControllerTest centerOfMassControllerTest;

    centerOfMassControllerTest.go();

    return 0;
}
