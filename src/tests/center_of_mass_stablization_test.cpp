/*
 * jacobian_test.cpp
 *
 *  Created on: Nov 21, 2012
 *      Author: somervil
 */

#include <re2uta/AtlasDebugPublisher.hpp>
#include <re2uta/center_of_mass_jacobian.hpp>
#include <re2uta/kinematics.hpp>
#include <re2uta/pinv.hpp>
#include <re2/kdltools/kdl_tools.h>
#include <re2/visutils/VisualDebugPublisher.h>

#include <robot_state_publisher/robot_state_publisher.h>
#include <tf/transform_broadcaster.h>

#include <sensor_msgs/JointState.h>
#include <visualization_msgs/Marker.h>

#include <ros/ros.h>

#include <eigen_conversions/eigen_kdl.h>

#include <kdl/tree.hpp>
#include <kdl/treefksolverpos_recursive.hpp>
#include <kdl_parser/kdl_parser.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>

#include <iostream>
#include <fstream>



boost::shared_ptr<re2::VisualDebugPublisher>   vizDebugPublisher;


class CenterOfMassVelocitySolverTest
{
    private:
        typedef std::map<std::string, double> JointPositionMap;

        ros::NodeHandle  m_node;
        KDL::Tree        m_tree;
        KDL::JntArray    m_jointPositions;
        JointPositionMap m_jointPositionMap;
        tf::TransformBroadcaster m_broadcaster;


    public:
        CenterOfMassVelocitySolverTest()
        {
            using namespace re2::drc::kinematics;

            std::string urdfFileName = "simple_arm.urdf" ;
            m_node.getParam( "urdf_file_name", urdfFileName );

            std::ifstream simpleUrdfFile( urdfFileName.c_str() );
            std::string   simpleUrdfString(  (std::istreambuf_iterator<char>(simpleUrdfFile)),
                                             (std::istreambuf_iterator<char>()              )  );

            m_node.setParam( "/robot_description", simpleUrdfString );

            kdl_parser::treeFromString( simpleUrdfString, m_tree );
            printTree( m_tree.getRootSegment()->second );


            m_jointPositions.resize( m_tree.getNrOfJoints() );
            ROS_INFO_STREAM( "Says we have " << m_tree.getNrOfJoints() << " joints" );

            int index = 0;
            BOOST_FOREACH( KDL::SegmentMap::value_type const & entry, m_tree.getSegments() )
            {
                KDL::Segment const & segment = entry.second.segment;
                m_jointPositionMap[ segment.getJoint().getName() ] = 0.1;
                m_jointPositions( entry.second.q_nr ) = 0.1;

                ++index;
            }
        }


        void go()
        {
            robot_state_publisher::RobotStatePublisher robotStatePublisher( m_tree );

            vizDebugPublisher.reset( new re2::VisualDebugPublisher( "jacobian_markers", "l_foot" ) );

            const KDL::TreeElement & rootElement      = m_tree.getRootSegment()->second;
            const KDL::TreeElement & leftFootElement  = m_tree.getSegment("l_foot" )->second;
            const KDL::TreeElement & rightFootElement = m_tree.getSegment("r_foot" )->second;
            const KDL::TreeElement & rlglutElement    = m_tree.getSegment("r_lglut")->second;
            double                   totalMass        = calcTreeMass(rootElement);

            int rightFootId = rightFootElement.q_nr;
            int rlglutId    = rlglutElement.q_nr;


            int counter = 0;
            while( ros::ok() )
            {
                Eigen::Affine3d    identity( Eigen::Matrix4d::Identity() );
                Eigen::Vector4d    leftPointMass(  0,0,0,0 );
                Eigen::Vector4d    rightPointMass( 0,0,0,0 );
                Eigen::MatrixXd    leftJacobian(      Eigen::MatrixXd::Ones(  3, m_tree.getNrOfJoints() ) * -1 );
                Eigen::MatrixXd    rightJacobian(     Eigen::MatrixXd::Ones(  3, m_tree.getNrOfJoints() ) * -1 );
                Eigen::MatrixXd    compositeJacobian( Eigen::MatrixXd::Ones(  3, m_tree.getNrOfJoints() ) * -1 );
                std::vector<bool>  leftVisited(  m_tree.getNrOfJoints(), 0 );
                std::vector<bool>  rightVisited( m_tree.getNrOfJoints(), 0 );
                Eigen::MatrixXd    invCenterOfMassJacobian;
                Eigen::VectorXd    jointVelocityVector;

                KDL::TreeFkSolverPos_recursive treeFkSolver( m_tree );

                {
                    leftVisited[  leftFootElement.q_nr  ] = true;
                    rightVisited[ rightFootElement.q_nr ] = true;

                    KDL::Frame footToPelvis;
                    KDL::Frame jointToPelvis;
                    treeFkSolver.JntToCart( m_jointPositions,  footToPelvis, "l_foot" );
                    treeFkSolver.JntToCart( m_jointPositions, jointToPelvis, "r_foot" );

                    KDL::Frame jointToFoot = footToPelvis.Inverse() * jointToPelvis;

                    Eigen::Affine3d rightToLeftTransform;
                    tf::transformKDLToEigen( jointToFoot, rightToLeftTransform );


                    calcComJacBackward( identity,             leftFootElement,  m_jointPositions.data,  leftJacobian,  leftPointMass, totalMass,  leftVisited );
                    calcComJacBackward( rightToLeftTransform, rightFootElement, m_jointPositions.data, rightJacobian, rightPointMass, totalMass, rightVisited );

                    Eigen::Vector3d rightFootInLeftFrame(0,0,0);
                    rightFootInLeftFrame = rightToLeftTransform * rightFootInLeftFrame;
                    ROS_WARN_STREAM( "Right in left " << rightFootInLeftFrame.transpose() );

                    int successiveElements = std::abs( rlglutId - rightFootId );
                    int startElement       = std::min( rlglutId,  rightFootId );

                    compositeJacobian = leftJacobian;
                    compositeJacobian.block( 0, startElement, 3, successiveElements ) = rightJacobian.block( 0, startElement, 3, successiveElements ) ;
                }


                Eigen::Vector3d targetPoint;
                targetPoint.x() = std::sin(counter * M_PI/100)*0.05;
                targetPoint.y() = std::cos(counter * M_PI/100)*0.05;
                targetPoint.z() = 0.9;

                Eigen::Vector3d cartVelcoityDesired(0,0,0);
                cartVelcoityDesired  = targetPoint - leftPointMass.head<3>();


                invCenterOfMassJacobian = pinv( leftJacobian ); //compositeJacobian );
                jointVelocityVector     = invCenterOfMassJacobian * cartVelcoityDesired;

                Eigen::Vector3d    cartVelocityOut = compositeJacobian * jointVelocityVector;

//                std::cout << "cartVelcoityDesired " << cartVelcoityDesired.transpose() << "\n";
//                std::cout << "cartVelocityOut     " << cartVelocityOut.transpose()     << "\n";
//                std::cout << "jacobian   \n";



                vizDebugPublisher->publishPoint3( leftPointMass.head<3>(), Eigen::Vector4d(0,1,0,1), 100000, "l_foot" );

                BOOST_FOREACH( KDL::SegmentMap::value_type const & entry, m_tree.getSegments() )
                {
                    const KDL::Segment  & segment      = entry.second.segment;

                    if( segment.getJoint().getType() != KDL::Joint::None )
                    {
                        int jointId = entry.second.q_nr;
                        Eigen::Vector3d rotationAxis;
                        rotationAxis = Eigen::Map<Eigen::Vector3d>( segment.getJoint().JointAxis().data );
                        rotationAxis.normalize();

                        Eigen::Vector3d scaledRotationAxis = rotationAxis * jointVelocityVector(jointId); // / 10.0;
                        vizDebugPublisher->publishVector( scaledRotationAxis, Eigen::Vector4d(0,1,1,1), 200 + jointId, segment.getName() );

                        std::cout << "Joint " << segment.getName() << " " << compositeJacobian.col(jointId).transpose() << "\n";

                        KDL::Frame footToPelvis;
                        KDL::Frame jointToPelvis;
                        treeFkSolver.JntToCart( m_jointPositions,  footToPelvis,          "l_foot" );
                        treeFkSolver.JntToCart( m_jointPositions, jointToPelvis, segment.getName() );

                        KDL::Frame jointToFoot = footToPelvis.Inverse() * jointToPelvis;

                        vizDebugPublisher->publishVectorFrom( Eigen::Vector3dMap( jointToFoot.p.data ),  compositeJacobian.col(jointId), Eigen::Vector4d(0,1,0,1), 7000+jointId, "l_foot" );

                        m_jointPositions(jointId)                          -= jointVelocityVector(jointId) * 0.05;
                        m_jointPositionMap[ segment.getJoint().getName() ]  = m_jointPositions(jointId);
                    }

                }

                Eigen::Vector3d com     = leftPointMass.head<3>();

                vizDebugPublisher->publishSegment(            com,           targetPoint, Eigen::Vector4d(1,0,1,1),   1, "l_foot" );
//                vizDebugPublisher->publishVectorFrom(         com,  measuredCartVelocity, Eigen::Vector4d(0,0.5,0,1), 2, "l_foot" );
//                vizDebugPublisher->publishVectorFrom(         com,   cartVelcoityDesired, Eigen::Vector4d(0,1,0,1),   3, "l_foot" );
//                vizDebugPublisher->publishVectorFrom(         com,       cartVelocityOut, Eigen::Vector4d(0,1,0,1),   3, "l_foot" );
                vizDebugPublisher->publishPoint3(     targetPoint,                        Eigen::Vector4d(0,0,1,1),   4, "l_foot" );
                vizDebugPublisher->publishPoint3(             com,                        Eigen::Vector4d(1,1,0,1),   5, "l_foot" );


                ros::Duration(0.1).sleep();
                robotStatePublisher.publishFixedTransforms();
                robotStatePublisher.publishTransforms( m_jointPositionMap, ros::Time::now() );

                ++counter;
            }
        }

};


int main( int argc, char ** argv )
{
    ros::init( argc, argv, "re2_drc_kinematic_test" );
    ros::NodeHandle node;

    CenterOfMassVelocitySolverTest centerOfMasVelocitySolverTest;

    centerOfMasVelocitySolverTest.go();

    return 0;
}
