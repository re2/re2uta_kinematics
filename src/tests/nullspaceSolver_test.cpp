/*
 * FullBodyPoseSolverTest.cpp
 *
 *  Created on: Mar 14, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/nullspaceSolver.hpp>

#include <re2uta/colors.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>

#include <robot_state_publisher/robot_state_publisher.h>
#include <re2/visutils/VisualDebugPublisher.h>
#include <re2uta/AtlasDebugPublisher.hpp>

#include <tf/transform_broadcaster.h>

#include <re2/kdltools/kdl_tools.h>

#include <kdl_parser/kdl_parser.hpp>
#include <urdf_model/model.h>

#include <ros/ros.h>

#include <iostream>
#include <fstream>


double nonNaN( const double & value )
{
    if( std::isinf(value) || std::isnan(value) )
        return 0;
    else
        return value;
}

boost::shared_ptr<re2::VisualDebugPublisher>    vizDebugPublisher;
boost::shared_ptr<re2uta::AtlasDebugPublisher>  atlasDebugPublisher;

using namespace re2uta;

class FullBodyPoseSolverTest
{
    private:
        typedef std::map<std::string, double> JointPositionMap;

        ros::NodeHandle  m_node;
        KDL::Tree        m_tree;
        urdf::Model      m_urdfModel;

        Eigen::VectorXd  m_jointPositions;
        Eigen::VectorXd  m_defaultJointPositions;
        Eigen::VectorXd  m_jointMins;
        Eigen::VectorXd  m_jointMaxs;
        JointPositionMap m_jointPositionMap;

        double           m_comZ;
        double           m_comY;
        double           m_legBend;
        double           m_footX;
        double           m_footZ;

        tf::TransformBroadcaster                     m_broadcaster;


    public:
        FullBodyPoseSolverTest()
        {
            std::string urdfFileName = "simple_arm.urdf" ;
            m_node.getParam( "urdf_file_name", urdfFileName );

            m_comZ = 0.93;
            m_node.getParam( "com_z", m_comZ );

            m_comY = -0.084;
            m_node.getParam( "com_y", m_comY );

            m_legBend = 0.4;
            m_node.getParam( "leg_bend", m_legBend );

            m_footX = 0.24;
            m_node.getParam( "foot_x", m_footX );

            m_footZ = 0.05;
            m_node.getParam( "foot_z", m_footZ );

            std::ifstream simpleUrdfFile( urdfFileName.c_str() );
            std::string   simpleUrdfString(  (std::istreambuf_iterator<char>(simpleUrdfFile)),
                                             (std::istreambuf_iterator<char>()              )  );


            m_node.setParam( "/robot_description", simpleUrdfString );

            m_urdfModel.initString( simpleUrdfString );

            re2::kdltools::JointNameToAngleMap jointMins;
            re2::kdltools::JointNameToAngleMap jointMaxs;

            re2::kdltools::getJointLimits( m_urdfModel, jointMins, jointMaxs );


            kdlRootInertiaWorkaround( simpleUrdfString, m_tree );
            printTree( m_tree );

            m_jointPositions = Eigen::VectorXd::Ones( m_tree.getNrOfJoints() ) *   std::numeric_limits<double>::quiet_NaN();
            m_jointMins      = Eigen::VectorXd::Ones( m_tree.getNrOfJoints() ) * - std::numeric_limits<double>::infinity();
            m_jointMaxs      = Eigen::VectorXd::Ones( m_tree.getNrOfJoints() ) *   std::numeric_limits<double>::infinity();

            ROS_INFO_STREAM( "Says we have " << m_tree.getNrOfJoints() << " joints" );

            int index = 0;
            BOOST_FOREACH( KDL::SegmentMap::value_type const & entry, m_tree.getSegments() )
            {
                KDL::Segment const & segment = entry.second.segment;
                m_jointPositionMap[ segment.getJoint().getName() ] = 0.0;
                m_jointPositions( entry.second.q_nr ) = 0.0;

                ROS_INFO_STREAM( "segmentName: " << segment.getName() << " jointName: " << segment.getJoint().getName() << " Qnr: " << entry.second.q_nr );

                re2::kdltools::JointNameToAngleMap::iterator minEntry;
                minEntry = jointMins.find( segment.getJoint().getName() );
                if( minEntry != jointMins.end() )
                {
                    m_jointMins( entry.second.q_nr ) = minEntry->second;
                    ROS_INFO_STREAM( "min: " << minEntry->second );
                }

                re2::kdltools::JointNameToAngleMap::iterator maxEntry;
                maxEntry = jointMaxs.find( segment.getJoint().getName() );
                if( maxEntry != jointMaxs.end() )
                {
                    m_jointMaxs( entry.second.q_nr ) = maxEntry->second;
                    ROS_INFO_STREAM( "max: " << maxEntry->second );
                }

                ++index;
            }


            m_defaultJointPositions = ((m_jointMaxs + m_jointMins)/2).unaryExpr( std::ptr_fun( nonNaN ) );
            m_defaultJointPositions( m_tree.getSegment( "l_lleg"  )->second.q_nr ) =  m_legBend * 2;
            m_defaultJointPositions( m_tree.getSegment( "r_lleg"  )->second.q_nr ) =  m_legBend * 2;
            m_defaultJointPositions( m_tree.getSegment( "l_talus" )->second.q_nr ) = -m_legBend;
            m_defaultJointPositions( m_tree.getSegment( "r_talus" )->second.q_nr ) = -m_legBend;
            m_defaultJointPositions( m_tree.getSegment( "l_uleg"  )->second.q_nr ) = -m_legBend - 0.02;
            m_defaultJointPositions( m_tree.getSegment( "r_uleg"  )->second.q_nr ) = -m_legBend - 0.02;
            m_defaultJointPositions( m_tree.getSegment( "l_uglut" )->second.q_nr ) =  0;
            m_defaultJointPositions( m_tree.getSegment( "r_uglut" )->second.q_nr ) =  0;
            m_defaultJointPositions( m_tree.getSegment( "l_lglut" )->second.q_nr ) =  0;
            m_defaultJointPositions( m_tree.getSegment( "r_lglut" )->second.q_nr ) =  0;
            m_defaultJointPositions( m_tree.getSegment( "r_scap"  )->second.q_nr ) =  1.3;
            m_defaultJointPositions( m_tree.getSegment( "l_scap"  )->second.q_nr ) = -1.3;

            m_jointPositions = m_defaultJointPositions;

        }

        void go()
        {
            robot_state_publisher::RobotStatePublisher robotStatePublisher( m_tree );
            vizDebugPublisher.reset( new re2::VisualDebugPublisher( "com_position_markers", "l_foot" ) );
            atlasDebugPublisher.reset( new re2uta::AtlasDebugPublisher(m_urdfModel) );

            KDL::TreeFkSolverPos_recursive solver( m_tree );


//            std::string baseFoot( "l_foot" );
//            std::string moveFoot( "r_foot" );
            std::string baseFoot( "r_foot" );
            std::string moveFoot( "l_foot" );
            atlasDebugPublisher->setBase(baseFoot);
            vizDebugPublisher->setDefaultFrame(baseFoot);

            ROS_ASSERT_MSG(false, "Sorry Isura, I changed the solver and broke this : /" );
//            Eigen::Vector6d originalTipPose = FullBodyPoseSolver::getPose( solver, baseFoot, moveFoot, m_jointPositions );
            Eigen::Vector6d originalTipPose;// = FullBodyPoseSolver::getPose( solver, baseFoot, moveFoot, m_jointPositions );
            ROS_WARN_STREAM( "originalTipPose: " << originalTipPose.transpose() );

            double sign =  -1;
            int counter =   0;
            int parts   = 100;
            while( ros::ok() )
            {
                if( counter % (parts) == 0 )
                {
                    ROS_INFO_STREAM( "switching count: " << counter << " remainder " << counter % 2*parts );
                    sign *= -1;
                    std::swap( baseFoot, moveFoot );
                    ROS_ASSERT_MSG(false, "Sorry Isura, I changed the solver and broke this : /" );
//                    originalTipPose = FullBodyPoseSolver::getPose( solver, baseFoot, moveFoot, m_jointPositions );
                    atlasDebugPublisher->setBase(baseFoot);
                }

//                const KDL::TreeElement * baseElement   = &(m_tree.getSegment( baseFoot )->second);
//                const KDL::TreeElement * pelvisElement = &(m_tree.getSegment( "utorso" )->second);
//                const KDL::TreeElement * pelvisElement = &(m_tree.getSegment( "pelvis" )->second);
//                const KDL::TreeElement * tipElement    = &(m_tree.getSegment( moveFoot )->second);

                Eigen::Vector3d desiredComPose( std::sin( counter/100.0 * M_PI )*m_footX/2 , sign * (std::cos( counter*2/100.0 * M_PI ) * m_comY), m_comZ );
//                Eigen::Vector3d desiredComPose( 0, sign * m_comY, m_comZ );
                Eigen::Vector3d desiredPostureOrientation( 0, 0, 0 );
                Eigen::Vector6d desiredTipPose = originalTipPose;
                desiredTipPose.x() = /*std::max( 0.0,*/ std::sin( counter/(double)parts    * M_PI )*m_footX /*)*/;
                desiredTipPose.z() =   std::max( 0.0,   std::cos( counter/(double)parts    * M_PI )*m_footZ );
                desiredComPose.x() = /*std::max( 0.0,*/ std::sin( counter/(double)parts    * M_PI )*m_footX/2 /*)*/;
//                desiredTipPose.x() = std::sin( counter/100.0 )*0.25;
//                desiredTipPose(4) = 0.1; //(counter%100 - 50)/50;

                ROS_ASSERT_MSG(false, "Sorry Isura, I changed the solver and broke this : /" );
                Eigen::VectorXd currentPose;// = FullBodyPoseSolver::calcCurrentPose( solver, baseElement, pelvisElement, tipElement, m_jointPositions, Eigen::Affine3d::Identity() );
                Eigen::VectorXd jointPose  ;// = FullBodyPoseSolver::solveForPose( m_tree,
//                                                                                                        baseFoot,
//                                                                                                        moveFoot,
//                                                                                                        m_jointPositions,
//                                                                                                        desiredTipPose,
//                                                                                                        desiredPostureOrientation,
//                                                                                                        desiredComPose,
//                                                                                                        m_defaultJointPositions,
//                                                                                                        m_jointMins,
//                                                                                                        m_jointMaxs  );
//                Eigen::VectorXd jointPose = m_jointPositions.data;
                vizDebugPublisher->publishPoint3(desiredComPose,Red,99999999);
//                vizDebugPublisher->publishPoint3(currentPose.head(3),Green,99999989);


//                getchar();
                if( jointPose.rows() == 0 )
                {
                    ROS_ERROR( "empty solution" );
                    jointPose = m_jointPositions;
//                    continue;
                }
                else
                {
//                    ROS_INFO_STREAM( "solution for footX " << desiredTipPose.x() << " is:\n" << jointPose );
                }

                BOOST_FOREACH( KDL::SegmentMap::value_type const & entry, m_tree.getSegments() )
                {
                    const KDL::Segment  & segment      = entry.second.segment;

                    if( segment.getJoint().getType() != KDL::Joint::None )
                    {
                        int jointId = entry.second.q_nr;
                        Eigen::Vector3d rotationAxis;
                        rotationAxis = Eigen::Map<Eigen::Vector3d>( segment.getJoint().JointAxis().data );

                        m_jointPositions(jointId)                           = jointPose(jointId);
                        m_jointPositionMap[ segment.getJoint().getName() ]  = m_jointPositions(jointId);
                    }

                }

                ros::Duration(0.002).sleep();
                robotStatePublisher.publishFixedTransforms();
                robotStatePublisher.publishTransforms( m_jointPositionMap, ros::Time::now() );

                ++counter;
            }
        }

};


int main( int argc, char ** argv )
{
    ros::init( argc, argv, "re2uta_kinematics_nullspacesolver_test" );
    ros::NodeHandle node;

    FullBodyPoseSolverTest fullBodyPoseSolverTest;

    fullBodyPoseSolverTest.go();

    return 0;
}
