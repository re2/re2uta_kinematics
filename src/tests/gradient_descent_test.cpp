/*
 * gradient_descent_test.cpp
 *
 *  Created on: Apr 3, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/gradient_descent.hpp>

#include <iostream>

#include <ros/ros.h>

class GradientDescentTest
{
    public:
        void go()
        {
            int numInputs = 50;
            Eigen::VectorXd mins    = Eigen::VectorXd::Ones(numInputs) * -1;
            Eigen::VectorXd maxs    = Eigen::VectorXd::Ones(numInputs) *  1;
            Eigen::VectorXd centers = Eigen::VectorXd::Ones(numInputs) *  0.1;
            Eigen::VectorXd range = maxs - mins;
            Eigen::VectorXd inputValues(numInputs);
            for( int i = 0; i < inputValues.rows(); ++i )
            {
                inputValues(i) = mins(i) + range(i)/inputValues.rows() * i;
            }


            double penaltyRangeFactor = 0.5;

            Eigen::VectorXd intrusion = calcNormalizedIntrusion( mins, maxs, centers, inputValues, penaltyRangeFactor );
            Eigen::VectorXd penalty   = calcPenalty(             mins, maxs, centers, inputValues, penaltyRangeFactor );

            for( int i = 0; i < inputValues.rows(); ++i )
            {
                std::cout << i << " min: " << mins(i) << " value: " << inputValues(i) << " maxs: " << maxs(i)
                               << " intrusion: " << intrusion(i) << " penalty: " << penalty(i) << "\n" ;
            }
        }
};


int main( int argc, char ** argv )
{
    ros::init( argc, argv, "gradient_descent_test" );
    ros::NodeHandle node;

    GradientDescentTest gradientDescentTest;

    gradientDescentTest.go();

    return 0;
}


