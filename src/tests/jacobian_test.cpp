/*
 * jacobian_test.cpp
 *
 *  Created on: Nov 21, 2012
 *      Author: somervil
 */


#include <re2uta/kinematics.hpp>
#include <re2uta/pinv.hpp>
#include "../velocity_solver.cpp"
#include "../CenterOfMassVelocitySolver.cpp"

#include <sensor_msgs/JointState.h>

#include <re2/kdltools/kdl_tools.h>

#include <visualization_msgs/Marker.h>

#include <re2/visutils/VisualDebugPublisher.h>

#include <ros/ros.h>

#include <kdl/segment.hpp>
#include <kdl/joint.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>

#include <iostream>
#include <fstream>

int main( int argc, char ** argv )
{
    using namespace re2::drc::kinematics;

    ros::init( argc, argv, "re2_drc_kinematic_test" );
    ros::NodeHandle node;

    std::string urdfFileName = "simple_arm.urdf" ;
    node.getParam( "urdf_file_name", urdfFileName );

    std::ifstream simpleUrdfFile( urdfFileName.c_str() );
    std::string   simpleUrdfString(  (std::istreambuf_iterator<char>(simpleUrdfFile)),
                                     (std::istreambuf_iterator<char>()              )  );

    node.setParam( "/robot_description", simpleUrdfString );

    KDL::Tree tree;
    kdl_parser::treeFromString( simpleUrdfString, tree );

    KDL::Chain chain;
//    KDL::Chain condensedChain;

    tree.getChain( tree.getRootSegment()->second.segment.getName(), "arm_wrist_roll", chain );

//    re2::kdltools::buildChain( simpleUrdfString, tree.getRootSegment().getName(), tree.getRootSegment().getName() );

    std::cout << "num segments " << chain.getNrOfSegments() << "\n";
    std::cout << "num joints   " << chain.getNrOfJoints()   << "\n";

    sensor_msgs::JointState jointStateMsg;
    jointStateMsg.name.resize(getNrOfJoints());
    jointStateMsg.position.resize(getNrOfJoints());

    KDL::Jacobian jacobian0;
    KDL::JntArray jointPositions( chain.getNrOfJoints() );

    BOOST_FOREACH( const KDL::Segment & segment, chain.segments )
    {
        Eigen::Vector3d offset = Eigen::Map<Eigen::Vector3d>( segment.getFrameToTip().p.data, 3    );
        Eigen::Matrix3d basis  = Eigen::Map<Eigen::Matrix3d>( segment.getFrameToTip().M.data, 3, 3 );

        std::cout << "  offset     " << offset.transpose()               << "\n";
        std::cout << "  basis      " << basis.row(0)                     << "\n";
        std::cout << "  basis      " << basis.row(1)                     << "\n";
        std::cout << "  basis      " << basis.row(2)                     << "\n";
        std::cout << "  name       " << segment.getName()                << "\n";
        std::cout << "  joint name " << segment.getJoint().getName()     << "\n";
        std::cout << "  joint type " << segment.getJoint().getTypeName() << "\n";
        std::cout << "  \n" ;

    }



    KDL::ChainJntToJacSolver jntToJacSolver( chain );


    jacobian0.resize( chain.getNrOfJoints() );

    if( -1 == jntToJacSolver.JntToJac( jointPositions, jacobian0 ) )
        std::cout << "ERROR in JntToJac\n";
    else
    {
        std::cout << "JntToJac size: " << jacobian0.rows() << "\n";
    }

    std::vector<bool> jointLockState( chain.getNrOfJoints() );
    int numUnlockedJoints = chain.getNrOfJoints();

    KDL::Jacobian jacobian1;
    jacobian1 = calculateJacobian( chain, jointPositions, jointLockState, numUnlockedJoints );

    Eigen::Matrix<double,6,1> twist;
    Eigen::Matrix<double,Eigen::Dynamic,1> solutionVector;
    Eigen::Matrix<double,6,6> jacobianMat;



//    solutionVector = computeOutputDirectionInInputSpace( jacobian1.data, twist );


    std::cout << "jacobian dims: " << jacobian1.data.cols() << " x " << jacobian1.data.rows() << "\n";

    solutionVector = pinv( jacobian1.data ) * twist;

//    KDL::Jacobian jacobian2;
//    jacobian2 = calculateJacobianFromCondensedChain( chain, jointPositions );

    std::cout << "Jacobian 0\n";
    std::cout << jacobian0.data << "\n";
    std::cout << "\n";
    std::cout << "Jacobian 1\n";
    std::cout << jacobian1.data << "\n";
    std::cout << "\n";
    std::cout << "Solution\n";
    std::cout << solutionVector.transpose() << "\n";
    std::cout << "\n";

    re2::VisualDebugPublisher debugPublisher( "jacobian_markers" );


    std::vector<int>    noIgnores;

    Eigen::Vector4d com;
    com = calcCenterOfMassBeyond( &tree.getRootSegment()->second, jointPositions.data, noIgnores );

    while( ros::ok() )
    {
        ros::spinOnce();
        for( int i = 0; i < jacobian1.columns(); ++i )
        {
            debugPublisher.publishVectorFrom( Eigen::Vector3d(0,0,0), Eigen::Vector3d(jacobian1.data.col(i).head(3)), Eigen::Vector4d(0,0,1,1), 100+i );
        }
        debugPublisher.publishPoint3( com.head<3>(), Eigen::Vector4d(1,0,0,1), 3 );
        ros::Duration(0.5).sleep();
    }

    return 0;
}
