/*
 * calc_com.cpp
 *
 *  Created on: Mar 4, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/calc_point_mass.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2/eigen/eigen_util.h>

#include <kdl/tree.hpp>

#include <boost/foreach.hpp>
#include <set>
#include <map>




Eigen::Vector4d calcPointMassOld( const KDL::Tree & tree,
                                  const KDL::TreeElement & element,
                                  const Eigen::VectorXd &  jointAngles )
{
    Eigen::Affine3d identity( Eigen::Affine3d::Identity() );
    std::set<const KDL::TreeElement*> visited;
    Eigen::Vector4d pointMass_out( Eigen::Vector4d::Zero() );

    calcPointMassBackward( tree, identity, element, jointAngles, pointMass_out, visited );
//    calcPointMassForward(  identity, element, jointAngles, pointMass_out, visited );
//    calcPointMass_internal( identity, identity, Eigen::Zero3d, element.children, element, jointAngles, pointMass_out, visited );

    return pointMass_out;
}


Eigen::Vector4d calcPointMass(const KDL::Tree & tree,
                               const KDL::TreeElement & element,
                               const Eigen::VectorXd &  jointAngles )
{
    return calcPointMass1( tree, element, jointAngles );
//    return calcPointMassOld( element, jointAngles );
}

Eigen::Vector4d calcPointMass0(const KDL::Tree & tree,
                               const KDL::TreeElement & element,
                               const Eigen::VectorXd &  jointAngles )
{
    Eigen::Affine3d identity( Eigen::Affine3d::Identity() );
    std::vector<bool> visited( jointAngles.rows(), false );
    Eigen::Vector4d pointMass_out( Eigen::Vector4d::Zero() );

    pointMass_out = recursivePointMass( element, tree, jointAngles, identity, visited );


    return pointMass_out;
}




Eigen::Vector4d recursivePointMassWithSet( const KDL::TreeElement & element,
                                    const KDL::Tree & tree,
                                    const Eigen::VectorXd & jointAngles,
                                    const Eigen::Affine3d & toParent,
                                    std::set<const KDL::TreeElement*> & visited );

Eigen::Vector4d calcPointMass1(const KDL::Tree & tree,
                               const KDL::TreeElement & element,
                               const Eigen::VectorXd &  jointAngles )
{
    Eigen::Affine3d identity( Eigen::Affine3d::Identity() );
    std::set<const KDL::TreeElement*> visited;
    Eigen::Vector4d pointMass_out( Eigen::Vector4d::Zero() );

    pointMass_out = recursivePointMassWithSet( element, tree, jointAngles, identity, visited );

    return pointMass_out;
}


void calcPointMass_internal( const KDL::Tree &        tree,
                             const Eigen::Affine3d &  toLowerFrame,
                             const Eigen::Affine3d &  toBaseFrame,
                             const Eigen::Vector3d &  jointLocation,
                             const KdlElementChildren & children,
                             const KDL::TreeElement & element,
                             const Eigen::VectorXd &  jointAngles,
                             const Eigen::Vector4d &  localPointMass,
                             Eigen::Vector4d &        postPointMass_out,
                             const int                jointIndex,
                             std::set<const KDL::TreeElement*> & visited_out )
{
    BOOST_FOREACH( const KDL::SegmentMap::const_iterator & itor, children )
    {
        const KDL::TreeElement & childElement = itor->second;

        if(    childElement.segment.getJoint().getType() == KDL::Joint::None  // FIXME this hack to get around KDL stupidity will likely cause an infinite loop eventually
            || visited_out.find( &childElement ) != visited_out.end() )
        {
            continue;
        }

        Eigen::Vector4d childPostPointMass(0,0,0,0);
        calcPointMassForward( tree, toBaseFrame, childElement, jointAngles,  childPostPointMass, visited_out );
        postPointMass_out = aggrigatePointMasses( childPostPointMass, postPointMass_out );
    }


    if(    element.segment.getJoint().getType() != KDL::Joint::None
        && visited_out.find( &element.parent->second ) == visited_out.end() )
    {
        Eigen::Vector4d childPostPointMass(0,0,0,0);
        calcPointMassBackward( tree, toBaseFrame, element, jointAngles, childPostPointMass, visited_out );
        postPointMass_out = aggrigatePointMasses( childPostPointMass, postPointMass_out );
    }


    Eigen::Vector3d centerOfMassInLocal;
    double          postMassProportion;

//    std::cout <<  "Integrating local mass of: " <<  localPointMass(3) << " for " << element.segment.getName() << " qnr " << element.q_nr << std::endl ;
    postPointMass_out     = aggrigatePointMasses( postPointMass_out, localPointMass  );
    centerOfMassInLocal   = postPointMass_out.head<3>();
    postPointMass_out.head<3>()    = toLowerFrame * postPointMass_out.head<3>();

    return;
}

void calcPointMassBackward( const KDL::Tree &        tree,
                            const Eigen::Affine3d &  lastToBaseFrame,
                            const KDL::TreeElement & prevElement,
                            const Eigen::VectorXd &  jointAngles,
                            Eigen::Vector4d &        postPointMass_out,
                            std::set<const KDL::TreeElement*> &   visited_out )
{
    const KDL::TreeElement & element      = prevElement.parent->second;
    const KDL::Segment &     prevSegment  = prevElement.segment;
    const KDL::Segment &     segment      = element.segment;
    const KDL::Joint   &     joint        = prevSegment.getJoint();
    const KDL::Joint   &     nextJoint    = segment.getJoint();
    Eigen::Vector3d          jointAxis    = Eigen::Map< Eigen::Vector3d >( joint.JointAxis().data) ;
    int                      jointIndex   = prevElement.q_nr;


    if( joint.getType() != KDL::Joint::None  )
    {
        visited_out.insert( &prevElement );
    }

    Eigen::Vector3d jointOffset = Eigen::Vector3dMap( joint.pose(0).p.data );
    Eigen::Affine3d toBaseFrame = lastToBaseFrame;
    Eigen::Affine3d translation;
    Eigen::Affine3d rotation;
    Eigen::Affine3d toLowerFrame;

    translation   = Eigen::Translation3d( -jointOffset );
    rotation      = Eigen::AngleAxisd( -jointAngles( jointIndex ), jointAxis );
    toLowerFrame  = rotation * translation;
    toBaseFrame  = (toBaseFrame * toLowerFrame);

    Eigen::Vector4d localPointMass;
    localPointMass.head<3>()    = Eigen::Vector3dMap( segment.getInertia().getCOG().data );
    localPointMass.tail<1>()(0) = segment.getInertia().getMass();


    calcPointMass_internal( tree,
                            toLowerFrame,
                            toBaseFrame,
                            jointOffset,
                            element.children,
                            element,
                            jointAngles,
                            localPointMass,
                            postPointMass_out,
                            jointIndex,
                            visited_out );

    return;
}


void calcPointMassForward( const KDL::Tree &        tree,
                           const Eigen::Affine3d &  lastToBaseFrame,
                           const KDL::TreeElement & element,
                           const Eigen::VectorXd &  jointAngles,
                           Eigen::Vector4d &        postPointMass_out,
                           std::set<const KDL::TreeElement*> &    visited_out )
{
    const KDL::Segment &  segment    = element.segment;
    const KDL::Joint   &  joint      = segment.getJoint();
    Eigen::Vector3d       jointAxis  = Eigen::Map< Eigen::Vector3d >( joint.JointAxis().data) ;
    int                   jointIndex = element.q_nr;

    if( joint.getType()  != KDL::Joint::None )
        visited_out.insert( &element );

    Eigen::Vector3d jointOffset = Eigen::Vector3dMap( joint.pose(jointAngles(jointIndex)).p.data ); //FIXME warning this may not match my concept for what joint offset is

    Eigen::Affine3d toBaseFrame = lastToBaseFrame;
    Eigen::Affine3d toLowerFrame;
    toLowerFrame = Eigen::Translation3d( jointOffset );
    toLowerFrame = toLowerFrame * Eigen::AngleAxisd( jointAngles( jointIndex ), jointAxis ); // note post-multiply means reverse rotation

    toBaseFrame  = toBaseFrame * toLowerFrame;


    Eigen::Vector4d localPointMass;
    localPointMass.head<3>()    = Eigen::Vector3dMap( segment.getInertia().getCOG().data );
    localPointMass.tail<1>()(0) = segment.getInertia().getMass();

    calcPointMass_internal( tree,
                            toLowerFrame,
                            toBaseFrame,
                            Eigen::Zero3d,
                            element.children,
                            element,
                            jointAngles,
                            localPointMass,
                            postPointMass_out,
                            jointIndex,
                            visited_out );
}






//Eigen::Vector4d aggrigate( const Eigen::Vector4d & pm0, const Eigen::Vector4d & pm1 );
Eigen::Vector4d transformPointMass( const Eigen::Vector4d & pointMass, const Eigen::Affine3d & transform )
{
    Eigen::Vector4d transformedPointMass = pointMass;
    transformedPointMass.head(3) = transform * Eigen::Vector3d( pointMass.head(3) );
    return transformedPointMass;
}

Eigen::Vector4d getSegmentPointMass( const KDL::Segment & segment )
{
    Eigen::Vector4d pointMass;
    pointMass.head<3>()    = Eigen::Vector3dMap( segment.getInertia().getCOG().data );
    pointMass.tail<1>()(0) = segment.getInertia().getMass();

    return pointMass;
}

Eigen::Affine3d transformFromJoint( const KDL::Joint & joint, double jointAngle )
{
    Eigen::Affine3d transform;

    Eigen::Vector3d jointOffset = Eigen::Vector3dMap( joint.pose(jointAngle).p.data );
    transform = Eigen::Translation3d( jointOffset );

    Eigen::Vector3d jointAxis  = Eigen::Map< Eigen::Vector3d >( joint.JointAxis().data) ;
    transform = transform * Eigen::AngleAxisd( jointAngle, jointAxis );

    return transform;
}



Eigen::Vector4d recursivePointMass( const KDL::TreeElement & element,
                                    const KDL::Tree & tree,
                                    const Eigen::VectorXd & jointAngles,
                                    const Eigen::Affine3d & toParent,
                                    std::vector<bool> & visited )
{
    Eigen::Vector4d pointMassInParent(0,0,0,0);

    bool isRootSegment = element.segment.getName() == tree.getRootSegment()->first;
    if( isRootSegment || !visited[ element.q_nr ] )
    {
        Eigen::Vector4d pointMass(0,0,0,0);

        if( !isRootSegment )
        {
            visited[ element.q_nr ] = true;
            Eigen::Affine3d toThisFrame;
            toThisFrame = transformFromJoint( element.segment.getJoint(), jointAngles(element.q_nr) ).inverse();
            pointMass   = aggrigatePointMasses( pointMass, recursivePointMass( element.parent->second, tree, jointAngles, toThisFrame, visited ) );
        }

        BOOST_FOREACH( const KDL::SegmentMap::const_iterator & entry, element.children )
        {
            const KDL::TreeElement & childElement = entry->second;
            Eigen::Affine3d toThisFrame;
            toThisFrame = transformFromJoint( childElement.segment.getJoint(), jointAngles( childElement.q_nr ) );
            pointMass   = aggrigatePointMasses( pointMass, recursivePointMass( childElement, tree, jointAngles, toThisFrame, visited ) );
        }

        Eigen::Vector4d localPointMass;
        localPointMass    = getSegmentPointMass( element.segment );

        pointMass         = aggrigatePointMasses( pointMass, localPointMass );
        pointMassInParent = transformPointMass( pointMass, toParent );
    }

    return pointMassInParent;
}

Eigen::Vector4d recursivePointMassWithSet( const KDL::TreeElement & element,
                                    const KDL::Tree & tree,
                                    const Eigen::VectorXd & jointAngles,
                                    const Eigen::Affine3d & toParent,
                                    std::set<const KDL::TreeElement*> & visited )
{
    Eigen::Vector4d pointMassInParent(0,0,0,0);

    bool isRootSegment = element.segment.getName() == tree.getRootSegment()->first;
    if( visited.find( &element ) == visited.end() )
    {
//        std::cout << "not parent, continuing: " << element.segment.getName() << "\n";
        Eigen::Vector4d pointMass(0,0,0,0);

        visited.insert( &element );

        if( !isRootSegment )
        {
//            std::cout <<  "not parent, continuing: " << element.segment.getName() << "\n";
            Eigen::Affine3d toThisFrame;
            toThisFrame = transformFromJoint( element.segment.getJoint(), jointAngles(element.q_nr) ).inverse();
            pointMass   = aggrigatePointMasses( pointMass, recursivePointMassWithSet( element.parent->second, tree, jointAngles, toThisFrame, visited ) );
        }

        BOOST_FOREACH( const KDL::SegmentMap::const_iterator & entry, element.children )
        {
            const KDL::TreeElement & childElement = entry->second;
//            std::cout <<  "checking child: " << childElement.segment.getName() << "\n";
            Eigen::Affine3d toThisFrame;
            toThisFrame = transformFromJoint( childElement.segment.getJoint(), jointAngles( childElement.q_nr ) );
            pointMass   = aggrigatePointMasses( pointMass, recursivePointMassWithSet( childElement, tree, jointAngles, toThisFrame, visited ) );
        }

        Eigen::Vector4d localPointMass;
        localPointMass    = getSegmentPointMass( element.segment );

        pointMass         = aggrigatePointMasses( pointMass, localPointMass );
        pointMassInParent = transformPointMass( pointMass, toParent );
    }
    else
    {
//        std::cout <<  "already checked, bailing : " << element.segment.getName() << "\n";
    }

    return pointMassInParent;
}







