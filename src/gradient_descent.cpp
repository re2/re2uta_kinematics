/*
 * center_of_mass_position.cpp
 *
 *  Created on: Mar 14, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/debug.hpp>
#include <re2uta/gradient_descent.hpp>

#include <re2uta/calc_point_mass.hpp>
#include <re2uta/center_of_mass_jacobian.hpp>
#include <re2uta/tip_jacobian.hpp>
#include <re2uta/pinv.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>


#include <kdl/treefksolverpos_recursive.hpp>

#include <eigen_conversions/eigen_kdl.h>

#include <Eigen/Core>

#include <boost/function.hpp>
#include <boost/static_assert.hpp>

#include <iostream>
#include <ios>

namespace
{
    double nonNaN( const double & value )
    {
        if( std::isinf(value) || std::isnan(value) )
            return 0;
        else
            return value;
    }
}


Eigen::VectorXd
calcPenalty( const Eigen::VectorXd & mins,
             const Eigen::VectorXd & maxs,
             const Eigen::VectorXd & centers,
             const Eigen::VectorXd & inputValues,
             const double penaltyRangeFactor )
{
    Eigen::VectorXd result( inputValues.rows() );

    if(   (mins.rows() == inputValues.rows())
       && (maxs.rows() == inputValues.rows()) )
    {
        Eigen::VectorXd fullUpperRange           = (maxs    - centers);
        Eigen::VectorXd fullLowerRange           = (centers - mins);

        Eigen::VectorXd upperRange               = fullUpperRange * penaltyRangeFactor;
        Eigen::VectorXd lowerRange               = fullLowerRange * penaltyRangeFactor;

        Eigen::VectorXd normalizedUpperViolation = (inputValues - (maxs - upperRange)).array() / upperRange.array();
        Eigen::VectorXd normalizedLowerViolation = ((mins + lowerRange) - inputValues).array() / lowerRange.array();

        const Eigen::VectorXd zeros              = Eigen::VectorXd::Zero(inputValues.rows());
        Eigen::VectorXd upperResult              = -normalizedUpperViolation.cwiseMax(zeros).array().square(); // * penaltyRange.array();
        Eigen::VectorXd lowerResult              =  normalizedLowerViolation.cwiseMax(zeros).array().square(); // * penaltyRange.array();

        result = upperResult + lowerResult;
    }

    return result.unaryExpr( std::ptr_fun(nonNaN) );
}


Eigen::VectorXd
calcNormalizedIntrusion( const Eigen::VectorXd & mins,
                         const Eigen::VectorXd & maxs,
                         const Eigen::VectorXd & centers,
                         const Eigen::VectorXd & inputValues,
                         const double penaltyRangeFactor )
{
    Eigen::VectorXd result( inputValues.rows() );

    if(   (mins.rows() == inputValues.rows())
       && (maxs.rows() == inputValues.rows()) )
    {
        Eigen::VectorXd fullUpperRange           = (maxs    - centers);
        Eigen::VectorXd fullLowerRange           = (centers - mins);

        Eigen::VectorXd upperRange               = fullUpperRange * penaltyRangeFactor;
        Eigen::VectorXd lowerRange               = fullLowerRange * penaltyRangeFactor;

        Eigen::VectorXd normalizedUpperViolation = (inputValues - (maxs - upperRange)).array() / upperRange.array();
        Eigen::VectorXd normalizedLowerViolation = ((mins + lowerRange) - inputValues).array() / lowerRange.array();

        const Eigen::VectorXd zeros              = Eigen::VectorXd::Zero(inputValues.rows());
        Eigen::VectorXd upperResult              = normalizedUpperViolation.cwiseMax(zeros);
        Eigen::VectorXd lowerResult              = normalizedLowerViolation.cwiseMax(zeros);

        result = upperResult + lowerResult;
    }

    return result.unaryExpr( std::ptr_fun(nonNaN) );
}


Eigen::MatrixXd
solveGradientDescent( const Eigen::VectorXd & inputStateSeed,
                     JacobianFunctor &       generateJacobian,
                     FkSolveFunctor &        evaluateAt,
                     const Eigen::VectorXd & desiredOutputState,
                     GuardFunctor &          solutionReached,
                     const Eigen::VectorXd & minInputValues,
                     const Eigen::VectorXd & maxInputValues,
                     const Eigen::VectorXd & optimumInputValues,
                     const Eigen::MatrixXd & inputWeightMatrix,
                     const Eigen::MatrixXd & outputWeightMatrix )
{
    Eigen::VectorXd inputState         = inputStateSeed;
    Eigen::VectorXd currentOutputState = evaluateAt( inputState );
    Eigen::VectorXd outputSpaceGoalDiff;
    Eigen::VectorXd inputSpaceGoalDirection;
    Eigen::MatrixXd inputToOutputDerivXForm;
    Eigen::MatrixXd outputToInputDerivXForm;
    Eigen::MatrixXd nullSpace;

    Eigen::MatrixXd internalInputWeightMatrix  = inputWeightMatrix;
    Eigen::MatrixXd internalOutputWeightMatrix = outputWeightMatrix;
    Eigen::VectorXd step; // declared outside so we can use after loop

    int iterations = 0;
    while( ! solutionReached( currentOutputState, desiredOutputState, iterations ) ) //FIXME
    {
        inputToOutputDerivXForm  = Eigen::MatrixXd( generateJacobian( inputState )  );
        double lambda = 0.1;

        if( internalInputWeightMatrix.rows() > 0 || internalOutputWeightMatrix.rows() > 0 )
        {
            outputToInputDerivXForm  = Eigen::MatrixXd( weightedPseudoInverse( inputToOutputDerivXForm,
                                                                               internalInputWeightMatrix,
                                                                               internalOutputWeightMatrix, lambda ) );
//            internalInputWeightMatrix = inputWeightMatrix; //reset
        }
        else
            outputToInputDerivXForm  = Eigen::MatrixXd( pseudoInverse( inputToOutputDerivXForm, lambda ) );

        nullSpace = Eigen::MatrixXd::Identity(inputState.rows(),inputState.rows()) - outputToInputDerivXForm * inputToOutputDerivXForm;

        currentOutputState       = evaluateAt( inputState );
        outputSpaceGoalDiff      = desiredOutputState - currentOutputState;
        inputSpaceGoalDirection  = outputToInputDerivXForm * outputSpaceGoalDiff; //MEAT!


        double maxRadStep         = 0.01; // FIXME really bad for performance
        double maxSolutionElement = std::max( std::abs( inputSpaceGoalDirection.minCoeff() ), std::abs( inputSpaceGoalDirection.maxCoeff() ) );
        double movementFactor     = 1/maxSolutionElement * std::min( maxRadStep, (maxSolutionElement / 2) );  // normalize on MaxRadStep or smaller max element
        step                      = (inputSpaceGoalDirection * movementFactor);

        Eigen::VectorXd preStepInput = inputState; //for joint freezing
        inputState += step;

        Eigen::VectorXd rawPenalties      = calcPenalty( minInputValues, maxInputValues, optimumInputValues, inputState, 1 );
        Eigen::VectorXd adjustedPenalties = (nullSpace * rawPenalties) * movementFactor;
//        inputState += adjustedPenalties; // FIXME should we be scaling the penalty?
//        ROS_INFO_STREAM( "Penalties: " << penalties.transpose() );
        Eigen::VectorXd preLimitInput = inputState; //FIXME just for debug


//        Eigen::Matrix<bool,Eigen::Dynamic,1> rangeConformity = Eigen::Matrix<bool,Eigen::Dynamic,1>::Ones(inputState.rows());
        // Effectively joint limiting (nieve)
        if( minInputValues.rows() == inputState.rows() )
        {
//            rangeConformity.array() *= ((minInputValues - inputState).array() < Eigen::VectorXd::Zero( inputState.rows() ).array());
            inputState = inputState.cwiseMax( minInputValues );
        }


        if( maxInputValues.rows() == inputState.rows() )
        {
//            rangeConformity.array() *= (((inputState - maxInputValues).array() < Eigen::VectorXd::Zero( inputState.rows() ).array())).array();
            inputState = inputState.cwiseMin( maxInputValues );
        }


        Eigen::VectorXd preIntrusions = calcNormalizedIntrusion(minInputValues, maxInputValues, optimumInputValues, preStepInput,  1 );
        Eigen::VectorXd intrusions    = calcNormalizedIntrusion(minInputValues, maxInputValues, optimumInputValues, preLimitInput, 1 );
//        Eigen::VectorXd intrusions    = calcNormalizedIntrusion(minInputValues, maxInputValues, optimumInputValues, inputState,   1);
        Eigen::VectorXd intrusionDiff = intrusions - preIntrusions;

        Eigen::VectorXd wrongDirectionIntrusions = (intrusionDiff.array() >= 0).cast<double>() * intrusions.array().min( Eigen::ArrayXd::Ones(intrusions.rows()) );
        internalInputWeightMatrix = (Eigen::VectorXd::Ones(inputState.rows()) - wrongDirectionIntrusions).asDiagonal();
//        internalInputWeightMatrix = (Eigen::VectorXd::Ones(inputState.rows()) - intrusions).asDiagonal();

//        Eigen::VectorXd exponentialIntrusions = intrusions;
//        for( int i = 0; i < 2; ++i )
//            exponentialIntrusions.array().square();
//
//        internalOutputWeightMatrix.block(12,12,29,29) = exponentialIntrusions.asDiagonal()/2;
//        ROS_WARN_STREAM( "Intrusions: " << intrusions.transpose() );



        if( atlasDebugPublisher )
        {
            atlasDebugPublisher->publishQnrOrderedAngles( inputState );
        }

        if( vizDebugPublisher )
        {
            Eigen::VectorXd inputDiff = inputState - preLimitInput;
            if( (inputDiff.cwiseAbs().array() > 0).any() )
            {
//                ROS_WARN_STREAM( "JointCorrection Detected: " << std::setprecision(4) << std::fixed << inputDiff.transpose() );
                ROS_INFO( "" );
                ROS_WARN_STREAM( "JointCorrection Detected iteration: " << iterations );
//                ROS_WARN_STREAM( "wrongDirectionIntrusions: \n" << wrongDirectionIntrusions );
//                ROS_WARN_STREAM( "intrusion diff: \n" << intrusionDiff );
//                ROS_WARN_STREAM( "wrong way:      \n" << (intrusionDiff.array() > 0).cast<double>() );
//                ROS_WARN_STREAM( "inputWeightMatrixDiagonal: \n" << internalInputWeightMatrix.diagonal() );
//                ROS_WARN_STREAM( "raw inputWeightMatrixDiagonal: \n" << (Eigen::VectorXd::Ones(inputState.rows()) - intrusions) );


                for( int i = 0; i < inputDiff.rows(); ++i )
                {
                    if( std::abs( inputDiff(i) ) > 0 )
                    {
                        std::string jointName;
                        if( atlasDebugPublisher )
                            jointName = atlasDebugPublisher->lookup()->treeQnrToJointName(i);

                        ROS_WARN( "Joint (%s %i) Limit Hit (%2.3f,%2.3f), adjustments: %2.3f, currentValue: %2.3f",
                                   jointName.c_str(), i, minInputValues(i), maxInputValues(i), inputDiff(i), inputState(i) );

                        if( internalInputWeightMatrix.rows() > i )
                            ROS_WARN_STREAM( "Freeze was: "        << internalInputWeightMatrix(i,i)
                                          << " contribution was: " << step(i)
                                          << " penalty was: "      << adjustedPenalties(i) );
                    }
                }
            }


            Eigen::VectorXd approxOutDir        = (inputToOutputDerivXForm * inputSpaceGoalDirection);
            Eigen::VectorXd actualStepDir       = evaluateAt( inputState ) - currentOutputState; // current in this case was calculated pre-step. so is not current
            Eigen::VectorXd outputError         = outputSpaceGoalDiff - approxOutDir;
            Eigen::VectorXd weightedOutputError           = outputWeightMatrix * outputError;
            Eigen::VectorXd internallyWeightedOutputError = internalOutputWeightMatrix * outputError;

            if( (weightedOutputError).norm() > 0.01 )
            {
//                ROS_WARN_STREAM( "Weighted Single step solution off by > 0.01: " << std::setprecision(4) << std::fixed << (weightedOutputError).norm() );
//                ROS_WARN_STREAM( "Weighted Single step solution off by: " << std::setprecision(4) << std::fixed << outputError.transpose() );
//                for( int i = 0; i + 2 < currentOutputState.rows() && i < 12; i += 3 )
//                {
//                    ROS_WARN_STREAM( "Weighted Output triad "       << i / 3 << " off by " << std::setprecision(4) << std::fixed << weightedOutputError.block(i,0,3,1).norm() );
//                    ROS_WARN_STREAM( "Triad weight was: "          << outputWeightMatrix.diagonal().block(i,0,3,1).norm() );
//                    ROS_WARN_STREAM( "Triad internal weight was: " << internalOutputWeightMatrix.diagonal().block(i,0,3,1).norm() );
//                    ROS_WARN_STREAM( "" );
//                }
//
//                int lastSet = currentOutputState.rows() - 12;
//                ROS_WARN_STREAM( "Last " << lastSet << " Block off by "          << std::setprecision(4) << std::fixed << weightedOutputError.tail(lastSet).norm() );
//                ROS_WARN_STREAM( "Last " << lastSet << " Block internal off by " << std::setprecision(4) << std::fixed << internallyWeightedOutputError.tail(lastSet).norm() );
//                ROS_WARN_STREAM( "Last " << lastSet << " weight was: "           << outputWeightMatrix.diagonal().block(12,0,lastSet,1).norm() );
//                ROS_WARN_STREAM( "Last " << lastSet << " internal weight was: "  << internalOutputWeightMatrix.diagonal().block(12,0,lastSet,1).norm() );
//
//                ROS_WARN_STREAM( "turning on single step" );
////                debugSingleStep = true;
            }

            for( int i = 0; i + 2 < currentOutputState.rows() && i < 12; i += 3 )
            {
                double tripletWeight = outputWeightMatrix.block(i,0,3,1).diagonal().norm();
                Eigen::Array4d fade(1,1,1,0.7);
//                Eigen::Vector4d color = Magenta.array() * fade.array();
                Eigen::VectorXd scaledActualTriadStep = actualStepDir.block(i,0,3,1).normalized() *.1; // * approxOutDir.norm();
                Eigen::VectorXd scaledCalcedTriadStep = approxOutDir.block(i,0,3,1).normalized() *.1; // * approxOutDir.norm();

                int hash = BoostStringHash()("");

                vizDebugPublisher->publishPoint3(     desiredOutputState.block(i,0,3,1),                                       Green,        93857 + i/3 * 10 );
                vizDebugPublisher->publishPoint3(     currentOutputState.block(i,0,3,1),                                      Yellow,        93858 + i/3 * 10 );
                vizDebugPublisher->publishVectorFrom( currentOutputState.block(i,0,3,1), approxOutDir.block(i,0,3,1),           Cyan * fade, 93859 + i/3 * 10 );
                vizDebugPublisher->publishVectorFrom( currentOutputState.block(i,0,3,1), scaledCalcedTriadStep,             DarkCyan * fade, 93860 + i/3 * 10 );
                vizDebugPublisher->publishVectorFrom( currentOutputState.block(i,0,3,1), scaledActualTriadStep,              Magenta * fade, 93861 + i/3 * 10 );
                vizDebugPublisher->publishSegment(    currentOutputState.block(i,0,3,1), desiredOutputState.block(i,0,3,1),     Blue * fade, 93862 + i/3 * 10 );


//                vizDebugPublisher->publishVectorFrom( currentOutputState.block(i,0,3,1), approxOutDir.block(i,0,3,1),          Cyan, 93859 + i/3 * 4 );
//                vizDebugPublisher->publishVectorFrom( currentOutputState.block(i,0,3,1), approxOutDir.block(i,0,3,1),          Cyan, 93859 + i/3 * 4 );
            }

            if( debugSingleStep )
            {
                ROS_WARN_STREAM( "GoalSpaceDiff of "                << outputSpaceGoalDiff.norm()  );//<< " looked like\n" << outputSpaceGoalDiff );
                ROS_WARN_STREAM( "Weighted GoalDiff of "            << (outputWeightMatrix * outputSpaceGoalDiff).norm() );// << " looked like\n" << (outputWeightMatrix * outputSpaceGoalDiff) );
                ROS_WARN_STREAM( "internally Weighted GoalDiff of " << (internalOutputWeightMatrix * outputSpaceGoalDiff).norm() );// << " looked like\n" << (outputWeightMatrix * outputSpaceGoalDiff) );
                ROS_WARN_STREAM( "unadjusted input step of "        << inputSpaceGoalDirection.norm() );// << "at failure looked like: \n" << step );
                ROS_WARN_STREAM( "adjusted   input step of "        << step.norm() );//<< "at failure looked like: \n" << step );

                getchar();
            }
        }


//        ROS_WARN_STREAM( "input weight diagonal " << internalInputWeightMatrix.diagonal().transpose() );
//        if( (rangeConformity.array() == 0).any() )
//        {
//            internalInputWeightMatrix = rangeConformity.cast<double>().asDiagonal();
//            ROS_WARN( "Found non-conformant joint, setting inputWeights" );
//            ROS_WARN_STREAM( "rangeConformity: " << rangeConformity.transpose() );
//            ROS_WARN_STREAM( "rangeConformity: " << (minInputValues - inputState).array().transpose() );
//            inputState = preStepInput;
//        }

        ++iterations;
    }

    if( ! solutionReached( currentOutputState, desiredOutputState, -1 ) )
    {
        ROS_ERROR_STREAM( "Failed after " << iterations );
        ROS_WARN_STREAM( "GoalSpaceDiff of " << outputSpaceGoalDiff.norm()  );//<< " looked like\n" << outputSpaceGoalDiff );
        ROS_WARN_STREAM( "Weighted GoalDiff of " << (outputWeightMatrix * outputSpaceGoalDiff).norm() );// << " looked like\n" << (outputWeightMatrix * outputSpaceGoalDiff) );
        ROS_WARN_STREAM( "internally Weighted GoalDiff of " << (internalOutputWeightMatrix * outputSpaceGoalDiff).norm() );// << " looked like\n" << (outputWeightMatrix * outputSpaceGoalDiff) );
        ROS_WARN_STREAM( "unadjusted input step of " << inputSpaceGoalDirection.norm() );// << "at failure looked like: \n" << step );
        ROS_WARN_STREAM( "adjusted   input step of " << step.norm() );//<< "at failure looked like: \n" << step );
        return Eigen::VectorXd(); //FIXME is this what I really want to do?
//        return inputStateSeed;
    }
    else
    {
//        ROS_INFO_STREAM( "Solved in " << iterations << " iterations" << iterations );
//        for( int i=0; i< inputState.rows(); ++i )
//        {
//            std::string jointName = atlasDebugPublisher->lookup()->treeQnrToJointName(i);
//            ROS_INFO( "Joint (%s %i) solution %2.4f", jointName.c_str(), i, inputState(i) );
//        }
//        for( int i=0; i< inputState.rows(); ++i )
//        {
//            std::string jointName = atlasDebugPublisher->lookup()->treeQnrToJointName(i);
//            ROS_INFO( "Joint (%s %i) step %2.4f",jointName.c_str(), i, inputState(i) );
//        }
        return inputState;
    }
}










