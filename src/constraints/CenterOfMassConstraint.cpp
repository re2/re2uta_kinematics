/*
 * CenterOfMassConstraint.cpp
 *
 *  Created on: Apr 23, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/constraints/CenterOfMassConstraint.hpp>
#include <re2uta/constraints/SolverConstraint.hpp>

#include <re2uta/center_of_mass_jacobian.hpp>
#include <re2uta/calc_point_mass.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <rosconsole/macros_generated.h>

namespace re2uta
{

CenterOfMassConstraint::
CenterOfMassConstraint( int startRow, KdlTreeConstPtr tree, const KDL::TreeElement * baseElement )
    : CartesianConstraint( startRow, 3, tree, baseElement)
{
    m_treeMass    = calcTreeMass( *tree );
    m_goalWeights = Eigen::MatrixXd::Identity(dimensions(),dimensions());
    m_defaultTransform = Eigen::Affine3d::Identity();

    updateName();
}

CenterOfMassConstraint::
~CenterOfMassConstraint() {};


void
CenterOfMassConstraint::
updateName()
{
    if( m_baseElement )
        m_name = m_baseElement->segment.getName() + " to COM";
}


void
CenterOfMassConstraint::
setDefaultTransform( const Eigen::Affine3d & defaultTransform ) { m_defaultTransform = defaultTransform; }

Eigen::MatrixXd
CenterOfMassConstraint::
generateJacobian( const Eigen::VectorXd & currentInput )
{
    Eigen::MatrixXd jacobian;
    jacobian = calcTreeComJacobian( *m_tree, m_baseElement, currentInput, m_defaultTransform, m_treeMass );
    return jacobian;
}

Eigen::VectorXd
CenterOfMassConstraint::
getCurrentOutput( const Eigen::VectorXd & currentInput )
{
    Eigen::Vector4d currentPointMass;

    currentPointMass = calcPointMass( *m_tree, *m_baseElement, currentInput );

    return m_defaultTransform.inverse() * currentPointMass.head<3>(); // FIXME need to double check that this works as expected.
}


Eigen::VectorXd
CenterOfMassConstraint::
getPositionError( const Eigen::VectorXd & currentInput )
{
    Eigen::VectorXd positionError;
    positionError = goal() - getCurrentOutput( currentInput );
    return positionError;
}

Eigen::VectorXd
CenterOfMassConstraint::
getVelocityError( const Eigen::VectorXd & currentInput, const Eigen::VectorXd & currentInputDt )
{
    Eigen::VectorXd velocityError;
    //FIXME velocityError = desiredVelocity - getPositionDt();
    velocityError = Eigen::VectorXd::Zero( dimensions() );
    return velocityError;
}


bool
CenterOfMassConstraint::
solutionReached( const Eigen::VectorXd & fullCurrentOutput, int iterations )
{
    Eigen::Vector3d currentOutput = fullCurrentOutput.segment( startRow(), 3 );
    Eigen::Vector3d delta         = m_goalOutput - currentOutput;
    Eigen::Vector3d weightedDelta = (m_goalWeights * delta).array().abs();

    double epsilon = 0.005;
    if( (weightedDelta.array() < epsilon).all() )  // FIXME this should be parametrized somehow
    {
        return true;
    }
    else
    {
        if( iterations == -1 )
        {
            ROS_WARN_STREAM( "solution reach failure because row " << startRow() << " weighted comDelta : "  << weightedDelta.transpose() );
            ROS_WARN_STREAM( "solution reach failure because row " << startRow() << " comDelta : "           << delta.transpose()         );
        }

        return false;
    }
}

int
CenterOfMassConstraint::
dimensions() const
{
    return 3;
}



}
