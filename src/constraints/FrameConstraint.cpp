/*
 * FrameConstraint.cpp
 *
 *  Created on: Apr 23, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/constraints/FrameConstraint.hpp>
#include <re2uta/tip_jacobian.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <ros/ros.h>

namespace re2uta
{


FrameConstraint::
FrameConstraint( int startRow, KdlTreeConstPtr tree, const KDL::TreeElement * baseElement, const KDL::TreeElement * tipElement, KdlTreeFkSolverPtr fkSolver )
    : CartesianConstraint( startRow, 6, tree, baseElement, fkSolver )
{
    setTipElement( tipElement );
    m_defaultTransform = Eigen::Affine3d::Identity();
    m_goalWeights      = Eigen::MatrixXd::Identity(dimensions(),dimensions());

    if( baseElement )
        m_name = baseElement->segment.getName() + " to " + tipElement->segment.getName();
}


const std::string
FrameConstraint::
name()
{
    updateName();
    return m_name;
}


void
FrameConstraint::
updateName()
{
    std::string baseName = "NULL";
    std::string tipName  = "NULL";

    if( m_baseElement )
        baseName = m_baseElement->segment.getName();
    if( m_tipElement )
        tipName = m_tipElement->segment.getName();

    m_name = baseName + " to " + tipName;
}

//FrameConstraint::
//~FrameConstraint() {};
//
//void
//FrameConstraint::
//setDefaultTransform( const Eigen::Affine3d & defaultTransform ) { m_defaultTransform = defaultTransform; }



const KDL::TreeElement *
FrameConstraint::
setTipElement(       const std::string & frameName)
{
    try
    {
        m_tipElement = &getTreeElement( *m_tree, frameName );
    }
    catch( std::runtime_error & error )
    {
        ROS_ERROR( "Base tip element in Frame Constraint!" );
        m_tipElement = NULL;
    }
    return m_tipElement;
}

Eigen::MatrixXd
FrameConstraint::
generateJacobian( const Eigen::VectorXd & currentInput )
{
    Eigen::MatrixXd jacobian;
    jacobian = calcTreeTipJacobian( *m_tree, m_baseElement, m_tipElement, currentInput, m_defaultTransform );
    return jacobian;
}


Eigen::VectorXd
FrameConstraint::
getPositionError( const Eigen::VectorXd & currentInput )
{
    Eigen::VectorXd positionError( dimensions() );
    Eigen::VectorXd currentOutput = getCurrentOutput( currentInput );

    positionError    = goal() - currentOutput;

    if( positionError(3) > M_PI )
        positionError(3) = M_PI - positionError(3);

    if( positionError(4) > M_PI )
        positionError(4) = M_PI - positionError(4);

    if( positionError(5) > M_PI )
        positionError(5) = M_PI - positionError(5);

    return positionError;
}

Eigen::VectorXd
FrameConstraint::
getVelocityError( const Eigen::VectorXd & currentInput, const Eigen::VectorXd & currentInputDt )
{
    Eigen::VectorXd velocityError;
    //FIXME velocityError = desiredVelocity - getPositionDt();
    velocityError = Eigen::VectorXd::Zero( dimensions() );
    return velocityError;
}


Eigen::VectorXd
FrameConstraint::
getCurrentOutput( const Eigen::VectorXd & currentInput )
{
    Eigen::Vector6d cartesianPose;
    cartesianPose = getPose( m_baseElement->segment.getName(), m_tipElement->segment.getName(), currentInput );
    return cartesianPose;
}


bool
FrameConstraint::
solutionReached( const Eigen::VectorXd & fullCurrentOutput, int iterations )
{
    Eigen::Vector6d currentOutput = fullCurrentOutput.segment( startRow(), 6 );
    Eigen::Vector6d delta         = (m_goalOutput - currentOutput);
    Eigen::Vector6d weightedDelta = (m_goalWeights * delta).array().abs();


    double epsilon = 0.005;// FIXME this should be parametrized somehow
    if( (weightedDelta.array() < epsilon).all() )
    {
        return true;
    }
    else
    {
        if( iterations == -1 )
            ROS_WARN_STREAM( "solution reach failure because frame goal starting at " << startRow() << " delta is " << weightedDelta.transpose() );

        return false;
    }

    return false;
}

//int
//FrameConstraint::
//dimensions()
//{
//    return 6;
//}


Eigen::Vector6d
FrameConstraint::
getPose( const std::string & baseFrame,
         const std::string & tipFrame,
         const Eigen::VectorXd & jointPositions ) // FIXME should take default transform
{
    Eigen::Affine3d twistXform = getTransform( *m_fkSolver, baseFrame, tipFrame, jointPositions );

    Eigen::Vector6d twist;
    twist.head<3>() = twistXform.translation();
    twist.tail<3>() = twistXform.rotation().eulerAngles(0,1,2); //FIXME could this be a problem because of singularities??

    return twist;
}



Eigen::Vector6d affineToTwist( const Eigen::Affine3d transform )
{
    Eigen::Vector6d twist;

    twist.head<3>() = transform.translation();
    twist.tail<3>() = transform.rotation().eulerAngles(0,1,2); //FIXME could this be a problem because of singularities??

    return twist;
}



}
