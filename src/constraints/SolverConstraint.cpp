/*
 * SolverConstraint.hpp
 *
 *  Created on: Apr 19, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/constraints/SolverConstraint.hpp>
#include <boost/lexical_cast.hpp>

namespace re2uta
{

SolverConstraint::
SolverConstraint( int startRow, int dims )
{
    m_startRow    = startRow;
    m_dimensions  = dims;
    m_goalWeights = Eigen::MatrixXd::Identity(dims,dims);
    m_name        = boost::lexical_cast<std::string>( m_startRow );

    m_posKp       = Eigen::VectorXd::Ones(dims) * 1; // FIXME somewhat arbitrary default
    m_posKd       = Eigen::VectorXd::Ones(dims);// * 10; // FIXME somewhat arbitrary default
    m_velKp       = Eigen::VectorXd::Ones(dims);// * 10; // FIXME somewhat arbitrary default
    m_velMax      = Eigen::VectorXd::Ones(dims) * 10; // FIXME somewhat arbitrary default
};



int
SolverConstraint::
startRow()
{
    return m_startRow;
}

void
SolverConstraint::
setStartRow( int startRow )
{
    m_startRow = startRow;
}


void
SolverConstraint::
internal_setStartRow( int startRow )
{
    m_startRow = startRow;
}

void
SolverConstraint::
setGoal(             const Eigen::VectorXd & goalOutput   )
{
    m_goalOutput = goalOutput;
    assert( m_goalOutput.size() == m_dimensions );
}

Eigen::VectorXd &
SolverConstraint::
goal()
{
    return m_goalOutput;
}


Eigen::VectorXd
SolverConstraint::
getVelocityError( const Eigen::VectorXd & currentInput, const Eigen::VectorXd & currentInputDt )
{
    //FIXME currently don't have desired velocity
    //Eigen::VectorXd velocityError = currentOutputDeriv() - m_desiredVelocity
    Eigen::VectorXd velocityError;
    velocityError = Eigen::VectorXd::Zero( dimensions() );
    return velocityError;
}

Eigen::VectorXd
SolverConstraint::
getPositionError( const Eigen::VectorXd & currentInput )
{
    Eigen::VectorXd positionError;
    positionError = goal() - getCurrentOutput(currentInput);
    return positionError;
}

void
SolverConstraint::
setGoalWeightVector( const Eigen::VectorXd & goalWeights   )
{
    m_goalWeights.diagonal() = goalWeights;
}

Eigen::MatrixXd::DiagonalReturnType
SolverConstraint::
goalWeightVector()
{
    return m_goalWeights.diagonal();
}

void
SolverConstraint::
setGoalWeightMatrix( const Eigen::MatrixXd & goalWeights   )
{
    m_goalWeights = goalWeights;
}

Eigen::MatrixXd &
SolverConstraint::
goalWeightMatrix()
{
    return m_goalWeights;
}

const std::string
SolverConstraint::
name()
{
    return m_name;
}


}
