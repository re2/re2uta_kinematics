/*
 * zmp_publisher.cpp
 *
 *  Created on: Jan 21, 2013
 *      Author: somervil
 */


#include <re2uta/zmp.hpp>
#include <re2/eigen/eigen_util.h>
#include <re2/matrix_conversions.h>
#include <re2/visutils/VisualDebugPublisher.h>

#include <eigen_conversions/eigen_msg.h>

#include <tf/transform_listener.h>

#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Vector3Stamped.h>

#include <ros/ros.h>

#include <boost/foreach.hpp>


Eigen::Vector3d toEigen( const geometry_msgs::Vector3 & vector )
{
    typedef Eigen::Matrix<geometry_msgs::Vector3::_x_type, 3, 1> Vector3MatrixType;

    return Eigen::Vector3d( Eigen::Map<const Vector3MatrixType>( &vector.x ) ); // assume object layout is exactly x,y,z
}


geometry_msgs::Vector3::Ptr toMsg( const Eigen::Vector3d & vector )
{
    geometry_msgs::Vector3::Ptr vectorMsg( new geometry_msgs::Vector3 );

    vectorMsg->x = vector.x();
    vectorMsg->y = vector.y();
    vectorMsg->z = vector.z();

    return vectorMsg;
}


geometry_msgs::Vector3Stamped::Ptr toMsg( const Eigen::Vector3d & vector, const std::string & frameName )
{
    geometry_msgs::Vector3Stamped::Ptr vectorMsg( new geometry_msgs::Vector3Stamped );

    vectorMsg->vector.x = vector.x();
    vectorMsg->vector.y = vector.y();
    vectorMsg->vector.z = vector.z();

    vectorMsg->header.frame_id = frameName;

    return vectorMsg;
}



class SensorContext
{
    public:
        typedef boost::shared_ptr<SensorContext> Ptr;

        SensorContext( const std::string & ftTopic )
        {
            m_sensedForceN   = Eigen::Nan3d;
            m_sensedTorqueNM = Eigen::Nan3d;
            m_ftSub  = m_nh.subscribe( ftTopic, 1, &SensorContext::handleFtMsg, this );
        }

        void
        handleFtStampedMsg( const geometry_msgs::WrenchStampedConstPtr & msg )
        {
            m_frameName = msg->header.frame_id;
            handleFtMsg( msg->wrench );
        }

        void
        handleFtMsg( const geometry_msgs::Wrench & wrench )
        {
            using namespace re2uta;

            m_sensedForceN   = toEigen( wrench.force  );
            m_sensedTorqueNM = toEigen( wrench.torque );
            m_zeroMomentLine = calcZeroMomentLine( m_sensedForceN, m_sensedTorqueNM );
        }

        ros::NodeHandle           m_nh;
        Eigen::ParametrizedLine3d m_zeroMomentLine;
        Eigen::Vector3d           m_sensedForceN;
        Eigen::Vector3d           m_sensedTorqueNM;
        ros::Subscriber           m_ftSub;
        std::string               m_frameName;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


class ZmpPublisher
{
    public:
        ZmpPublisher()
            : m_visDebug( "debug" )
        {
            m_zmpPub     = m_nh.advertise<geometry_msgs::Vector3Stamped>( "zmp", 1 );
            m_floorPlane = Eigen::Hyperplane3d( Eigen::ZAxis3d, Eigen::Zero3d );

            SensorContext::Ptr sensor;
            sensor.reset( new SensorContext( "atlas/l_foot_contact" ) );
            sensor->m_frameName = "l_foot";
            m_ftSensors.push_back( sensor );

            sensor.reset( new SensorContext( "atlas/r_foot_contact" ) );
            sensor->m_frameName = "r_foot";
            m_ftSensors.push_back( sensor );

            m_frameName = "l_foot";

            m_publishTimer = m_nh.createTimer( ros::Duration( 0.01 ), &ZmpPublisher::publishZmp, this );
        }


        void publishZmp( const ros::TimerEvent & timerEvent )
        {
            using namespace re2uta;

            Eigen::Vector3d aggrigateForce( Eigen::Zero3d );
            Eigen::Vector3d aggrigateTorque( Eigen::Zero3d );

            BOOST_FOREACH( const SensorContext::Ptr & sensor, m_ftSensors )
            {
                Eigen::ParametrizedLine3d zeroMomentLine;
                zeroMomentLine = foreignToLocal( sensor->m_zeroMomentLine, sensor->m_frameName );

                Eigen::Vector3d sensorForce;
                sensorForce    = rotateForeignToLocal( sensor->m_sensedForceN, sensor->m_frameName );
                aggrigateForce  += sensorForce;
                aggrigateTorque += zeroMomentLine.origin().cross( sensorForce );
            }

            Eigen::ParametrizedLine3d aggrigateZeroMomentLine;
            aggrigateZeroMomentLine = calcZeroMomentLine( aggrigateForce, aggrigateTorque );

            m_zmp = re2::intersectLineWithPlane( aggrigateZeroMomentLine, m_floorPlane );

            if( !re2::is_nan(m_zmp) )
            {
                geometry_msgs::Vector3Stamped::Ptr zmpMsg;
                zmpMsg = toMsg( m_zmp, m_frameName );
                m_zmpPub.publish( zmpMsg );
//                m_visDebug.publishPoint3( m_zmp, Eigen::Vector4d(1,0,1,1), 1, m_frameName );
                m_visDebug.publishVectorFrom( m_zmp, Eigen::UniformScaling<double>(0.001) * aggrigateForce, Eigen::Vector4d(1,0,1,1), 1, m_frameName );
            }
        }

        Eigen::Vector3d rotateForeignToLocal( const Eigen::Vector3d & force, const std::string & lineFrameName )
        {
            Eigen::Affine3d foreignToLocalMat = lookupTransform( lineFrameName, m_frameName );

            return foreignToLocalMat.rotation() * force;
        }

        Eigen::ParametrizedLine3d foreignToLocal( const Eigen::ParametrizedLine3d & line, const std::string & lineFrameName )
        {
            Eigen::Affine3d foreignToLocalMat = lookupTransform( lineFrameName, m_frameName );

            Eigen::ParametrizedLine3d transformedLine = line;
            transformedLine.origin()    = foreignToLocalMat * transformedLine.origin();
            transformedLine.direction() = foreignToLocalMat * transformedLine.direction();

            return transformedLine;
        }

        Eigen::Affine3d lookupTransform( const std::string & fromFrame, const std::string & toFrame )
        {
            tf::StampedTransform tfTransform;
            m_tfListener.waitForTransform( toFrame, fromFrame, ros::Time(0), ros::Duration(2) );
            m_tfListener.lookupTransform( toFrame, fromFrame, ros::Time(0), tfTransform );

            Eigen::Matrix4d homoTransform = re2::convertTfToEigen4x4( tfTransform );

            return Eigen::Affine3d( homoTransform );
        }

    private:
        ros::NodeHandle                 m_nh;
        ros::Publisher                  m_zmpPub;
        Eigen::Vector3d                 m_zmp;
        ros::Timer                      m_publishTimer;
        std::vector<SensorContext::Ptr> m_ftSensors;
        Eigen::Hyperplane3d             m_floorPlane;
        std::string                     m_frameName;
        tf::TransformListener           m_tfListener;
        re2::VisualDebugPublisher       m_visDebug;


    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};






int main( int argc, char ** argv )
{
    ros::init( argc, argv, "zmp_publisher" );

    ZmpPublisher zmpPublisher;

    ros::spin();

    return 0;
}


