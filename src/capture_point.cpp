/**
 *
 * capture_point.cpp: ...
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created May 2, 2013
 * @modified May 2, 2013
 *
 */

#include <re2uta/capture_point.hpp>

namespace re2uta
{

Eigen::Vector3d // capture point xy only
calcInstantaneousCapturePoint( const Eigen::Vector3d & comPosition,  const Eigen::Vector3d & comVelocity, const double & comZ )
{
    const double gravityMPerSec = 9.81;
//  xcapture = xvelocity * sqrt( comZ/ gravity )

    // Projection matrix that projects the COM to the ground plane
    Eigen::MatrixXd P;
    P = Eigen::Vector3d(1,1,0).asDiagonal();

    // Instantaneous capture point
    // r_ic = Pr + r_dot*sqrt(z0/g)
    Eigen::Vector3d capturePoint = comPosition.array() + comVelocity.array() * std::sqrt( comZ / gravityMPerSec );

    capturePoint(2) = 0;

    return capturePoint;
}

}
