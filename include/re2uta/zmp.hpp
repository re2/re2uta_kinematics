/*
 * zmp.hpp
 *
 *  Created on: Jan 21, 2013
 *      Author: somervil
 */

#ifndef ZMP_HPP_
#define ZMP_HPP_

#include <re2/eigen/eigen_util.h>

#include <Eigen/Core>

namespace re2uta
{

Eigen::ParametrizedLine3d
calcZeroMomentLine( const Eigen::Vector3d & sensedForceN, const Eigen::Vector3d & sensedTorqueNM );

Eigen::Vector3d
calcZMP( const Eigen::Vector3d & origin, const Eigen::Vector3d & sensedForce, const Eigen::Vector3d & sensedTorque );

}


#endif /* ZMP_HPP_ */
