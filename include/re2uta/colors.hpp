/*
 * colors.hpp
 *
 *  Created on: Mar 21, 2013
 *      Author: andrew.somerville
 */

#ifndef COLORS_HPP_
#define COLORS_HPP_

#include <Eigen/Core>

extern const Eigen::Array4d Red;
extern const Eigen::Array4d Green;
extern const Eigen::Array4d Blue;
extern const Eigen::Array4d Yellow;
extern const Eigen::Array4d Magenta;
extern const Eigen::Array4d Cyan;
extern const Eigen::Array4d White;
extern const Eigen::Array4d Black;
extern const Eigen::Array4d DarkRed;
extern const Eigen::Array4d DarkGreen;
extern const Eigen::Array4d DarkBlue;
extern const Eigen::Array4d DarkYellow;
extern const Eigen::Array4d DarkMagenta;
extern const Eigen::Array4d DarkCyan;
extern const Eigen::Array4d Gray;

#endif /* COLORS_HPP_ */
