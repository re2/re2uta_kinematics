/*
 * capture_point.hpp
 *
 *  Created on: May 2, 2013
 *      Author: andrew.somerville
 */

#pragma once


#include <Eigen/Core>

namespace re2uta
{

Eigen::Vector3d // capture point xy only
calcInstantaneousCapturePoint( const Eigen::Vector3d & comPosition,  const Eigen::Vector3d & comVelocity, const double & comZ  );

}
