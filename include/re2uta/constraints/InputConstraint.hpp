/*
 * InputConstraint.hpp
 *
 *  Created on: Apr 23, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/constraints/SolverConstraint.hpp>
#include <kdl/tree.hpp>
#include <Eigen/Geometry>


namespace re2uta
{

class InputConstraint : public SolverConstraint
{
    public:
        typedef boost::shared_ptr<InputConstraint> Ptr;

    public:
        InputConstraint( int startRow, int dimensions );
        virtual ~InputConstraint();

        virtual Eigen::MatrixXd generateJacobian( const Eigen::VectorXd & currentInput );
        virtual Eigen::VectorXd getCurrentOutput( const Eigen::VectorXd & currentInput );
        virtual bool            solutionReached(  const Eigen::VectorXd & currentOutput, int iterations );
        virtual int             dimensions() const;

    private:
        int m_dimensions;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


}
