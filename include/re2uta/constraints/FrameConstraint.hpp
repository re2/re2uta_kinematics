/*
 * FrameConstraint.hpp
 *
 *  Created on: Apr 23, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/constraints/SolverConstraint.hpp>
#include <re2uta/constraints/CartesianConstraint.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2/eigen/eigen_util.h>



namespace re2uta
{

class FrameConstraint : public CartesianConstraint
{
    public:
        typedef boost::shared_ptr<FrameConstraint> Ptr;

    public:
        FrameConstraint( int startRow, KdlTreeConstPtr tree, const KDL::TreeElement * baseElement, const KDL::TreeElement * tipElement,  KdlTreeFkSolverPtr fkSolver = KdlTreeFkSolverPtr() );
        virtual ~FrameConstraint() {};

        void                     setTipElement(       const KDL::TreeElement * tipElement )      { m_tipElement       = tipElement;       }
        const KDL::TreeElement * setTipElement(       const std::string & frameName );
        void                     setDefaultTransform( const Eigen::Affine3d & defaultTransform ) { m_defaultTransform = defaultTransform; }

        virtual Eigen::MatrixXd  generateJacobian( const Eigen::VectorXd & currentInput );
        virtual Eigen::VectorXd  getCurrentOutput( const Eigen::VectorXd & currentInput );
        virtual Eigen::VectorXd  getPositionError( const Eigen::VectorXd & currentInput );
        virtual Eigen::VectorXd  getVelocityError( const Eigen::VectorXd & currentInput, const Eigen::VectorXd & currentInputDt );
        virtual bool             solutionReached(  const Eigen::VectorXd & currentOutput, int iterations );
        virtual int              dimensions() const { return 6; }
        const KDL::TreeElement * tipElement() const { return m_tipElement; }

        Eigen::Vector6d getPose( const std::string & baseFrame,
                                 const std::string & tipFrame,
                                 const Eigen::VectorXd & jointPositions );

        virtual const std::string name();

    protected:
        virtual void             updateName();


    private:
        const KDL::TreeElement * m_tipElement;
        Eigen::Affine3d          m_defaultTransform;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


Eigen::Vector6d affineToTwist( const Eigen::Affine3d transform );


}

