/*
 * nullspaceSolver.hpp
 *
 *  Created on: Apr 18, 2013
 *      Author: Isura Ranatunga
 */

#ifndef FULLBODYPOSESOLVERNULL_HPP_
#define FULLBODYPOSESOLVERNULL_HPP_

#include <kdl/tree.hpp>

#include <re2/kdltools/kdl_tree_util.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/shared_ptr.hpp>

#include <string>

namespace re2uta
{

class NullSpaceFullBodySolver
{
    public:
        typedef boost::shared_ptr<NullSpaceFullBodySolver> Ptr;

                static
                Eigen::VectorXd
                solveForPoseNull( const KDL::Tree       & tree,
                                  const std::string     & baseElementName,
                                  const std::string     & tipElementName,
                                  const Eigen::VectorXd & q,
                                  const Eigen::Vector6d & desiredTipPose,
                                  const Eigen::Vector3d & desiredPostureOrientation,
                                  const Eigen::Vector3d & desiredComPose,
                                  const Eigen::VectorXd & qDes,
                                  const Eigen::VectorXd & qMin = Eigen::VectorXd(),
                                  const Eigen::VectorXd & qMax = Eigen::VectorXd() );

                Eigen::VectorXd
                delW( const Eigen::VectorXd & q_min,
                                                     const Eigen::VectorXd & q_max,
                                                     const Eigen::VectorXd & q );

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW


};


}

#endif /* FULLBODYPOSESOLVERNULL_HPP_ */
