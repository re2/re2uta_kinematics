/*
 * tip_jacobian.h
 *
 *  Created on: Mar 1, 2013
 *      Author: andrew.somerville
 */

#ifndef TIP_JACOBIAN_H_
#define TIP_JACOBIAN_H_


#include <kdl/jacobian.hpp>
#include <kdl/chain.hpp>
#include <kdl/tree.hpp>
#include <kdl/jntarray.hpp>
#include <Eigen/Geometry>


namespace re2uta
{

KDL::Jacobian
calculateTipJacobian( const KDL::Chain &        chain,
                      const KDL::JntArray &     currentJointPositions,
                      const std::vector<bool> & lockedJoints,
                      const int                 numUnlockedJoints,
                      const KDL::Frame & defaultFrame = KDL::Frame::Identity() );

KDL::Jacobian
calculateTipJacobian( const KDL::Chain &        chain,
                      const Eigen::VectorXd&    currentJointPositions,
                      const std::vector<bool> & lockedJoints,
                      const int                 numUnlockedJoints,
                      const KDL::Frame & defaultFrame  = KDL::Frame::Identity() );

Eigen::MatrixXd
calcTreeTipJacobian( const KDL::Tree &        tree,
                     const KDL::TreeElement * baseElement,
                     const KDL::TreeElement * tipElement,
                     const Eigen::VectorXd &  treeJointPositions,
                     const Eigen::Affine3d &  defaultTransform = Eigen::Affine3d::Identity() );

}

#endif /* TIP_JACOBIAN_H_ */
